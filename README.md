## CALL CENTER  - ALMUNDO #

### Requisitos ###

* Java 8
* Maven
* Junit

##### Este proyecto fue creado con Intelij
##### Este proyecto cuenta con tests para probar la funcionalidad

### Descripcion

este proyecto consiste en resolver la atencion de llamadas de un call center,

* En la ruta "callcenter/src/main/resourses" se encuentra el diagrama de clases
* En la ruta "callcenter/src/main/java/com/almundo/domain" se encuentran los modelos de dominio que
dan solucion al problema.
 * En la rura "callcenter/src/main/java/com/almundo/services"
 se encuentra una clase servicio para ubicar los empleados disponibles
 
 * En la ruta "callcenter/src/main/java/com/almundo/infrastructure"
 se encuentra la clase dispatcher encargada de gestionar las llamadas


