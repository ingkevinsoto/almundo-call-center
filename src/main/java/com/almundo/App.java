package com.almundo;

import java.util.stream.IntStream;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        IntStream.rangeClosed(0, 2).forEach(a -> {
            System.out.println( "Hello World!" + a );
        });
    }
}
