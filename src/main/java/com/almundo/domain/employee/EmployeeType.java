package com.almundo.domain.employee;

import java.util.Arrays;

/**
 * @author Kevin
 */
public enum EmployeeType {

    OPERATOR("1", "OPERATOR"),
    SUPERVISOR("2", "SUPERVISOR"),
    MANAGER("3", "MANAGER");

    private final String code;
    private final String description;

    EmployeeType(String code, String description){
        this.code = code;
        this.description = description;
    }

    public static EmployeeType getEmployeeType(String code){
        return Arrays.stream(EmployeeType.values())
                .filter( status -> status.code.equalsIgnoreCase(code) )
                .findFirst().orElse(EmployeeType.OPERATOR);
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

}

