package com.almundo.domain.employee;

/**
 * @author Kevin
 */
public class Employee {

    private String name;
    private String idType;
    private int id;
    private EmployeeType employeeType;
    private EmployeeStatus employeeStatus;

    public Employee(String name, String idType, int id, EmployeeType employeeType, EmployeeStatus employeeStatus) {
        this.name = name;
        this.idType = idType;
        this.id = id;
        this.employeeType = employeeType;
        this.employeeStatus = employeeStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public EmployeeType getEmployeeType() {
        return employeeType;
    }

    public void setEmployeeType(EmployeeType employeeType) {
        this.employeeType = employeeType;
    }

    public EmployeeStatus getEmployeeStatus() {
        return employeeStatus;
    }

    public void setEmployeeStatus(EmployeeStatus employeeStatus) {
        this.employeeStatus = employeeStatus;
    }
}
