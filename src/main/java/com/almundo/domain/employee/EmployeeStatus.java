package com.almundo.domain.employee;

import java.util.Arrays;

/**
 * @author Kevin
 */
public enum EmployeeStatus {

    AVAILABLE("AVAILABLE", "AVAILABLE EMPLOYEE"),
    BUSY("BUSY", "BUSY EMPLOYEE");

    private final String code;
    private final String description;

    EmployeeStatus(String code, String description){
        this.code = code;
        this.description = description;
    }

    public static EmployeeStatus getEmployeeStatus(String code){
        return Arrays.stream(EmployeeStatus.values())
                .filter( status -> status.code.equalsIgnoreCase(code) )
                .findFirst().orElse(EmployeeStatus.BUSY);
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }
}
