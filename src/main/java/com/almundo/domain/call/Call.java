package com.almundo.domain.call;

import org.joda.time.DateTime;

/**
 * @author Kevin
 */
public class Call {
    private int duration;
    private CallStatus status;
    private DateTime dateWaiting;

    public Call(int duration, CallStatus status, DateTime dateWaiting) {
        this.duration = duration;
        this.status = status;
        this.dateWaiting = dateWaiting;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public CallStatus getStatus() {
        return status;
    }

    public void setStatus(CallStatus status) {
        this.status = status;
    }

    public DateTime getDateWaiting() {
        return dateWaiting;
    }

    public void setDateWaiting(DateTime dateWaiting) {
        this.dateWaiting = dateWaiting;
    }
}
