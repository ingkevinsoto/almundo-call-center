package com.almundo.domain.call;

import java.util.Arrays;

/**
 * @author Kevin
 */
public enum CallStatus {
    ANSWERED("ANSWERED", "CALL ANSWERED"),
    INCOMING("INCOMING", "CALL INCOMING"),
    WAITING("WAITING", "CALL WAITING");

    private final String code;
    private final String description;

    CallStatus(String code, String description){
        this.code = code;
        this.description = description;
    }

    public static CallStatus getCallStatus(String code){
        return Arrays.stream(CallStatus.values())
                        .filter( status -> status.code.equalsIgnoreCase(code) )
                        .findFirst().orElse(CallStatus.WAITING);
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    }


