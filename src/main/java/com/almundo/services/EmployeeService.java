package com.almundo.services;

import com.almundo.domain.employee.Employee;
import com.almundo.domain.employee.EmployeeStatus;
import com.almundo.domain.employee.EmployeeType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kevin
 */
public class EmployeeService {

    private static List<Employee> employees = new ArrayList<>();

    public static void fillEmployees(List<Employee> employeesToFill) {
        employees.clear();
        employees.addAll(employeesToFill);
    }

    private static boolean statusAvailable(Employee employee, EmployeeType employeeType){
        return employeeType.equals(employee.getEmployeeType()) && employee.getEmployeeStatus().equals(EmployeeStatus.AVAILABLE);
    }

    public static Employee findAvailableEmployee() {
        return employees
                .stream().filter(emp -> statusAvailable(emp, EmployeeType.OPERATOR))
                .findFirst()
                .orElse(employees.stream().filter(emp -> statusAvailable(emp, EmployeeType.SUPERVISOR))
                        .findFirst()
                        .orElse(employees.stream().filter(emp -> statusAvailable(emp, EmployeeType.MANAGER))
                                .findFirst()
                                .orElse(null)));
    }

}
