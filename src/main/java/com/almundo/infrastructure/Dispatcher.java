package com.almundo.infrastructure;

import com.almundo.domain.call.Call;
import com.almundo.domain.call.CallStatus;
import com.almundo.domain.employee.Employee;
import com.almundo.domain.employee.EmployeeStatus;
import com.almundo.services.EmployeeService;
import org.joda.time.DateTime;
import play.Logger;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author Kevin
 */
public class Dispatcher {

    private List<Call> callsWaiting;
    private int countCall = 0;


    public Dispatcher() {
        callsWaiting = new ArrayList<>();
    }


    public Call dispatchCall(Call call){
        Employee availableEmployee = EmployeeService.findAvailableEmployee();
        if (countCall < 10 && availableEmployee != null) {
            answerCall(call, availableEmployee);
            countCall += 1;
            retryCall();
        } else {
            call.setStatus(CallStatus.WAITING);
            call.setDateWaiting(DateTime.now());
            callsWaiting.add(call);
        }

        return call;
    }

    public void answerCall(Call call, Employee availableEmployee){
        availableEmployee.setEmployeeStatus(EmployeeStatus.BUSY);
        call.setStatus(CallStatus.ANSWERED);
        Runnable assignment = () -> {
            try {
                TimeUnit.SECONDS.sleep(call.getDuration());
                availableEmployee.setEmployeeStatus(EmployeeStatus.AVAILABLE);
                countCall -= 1;
            } catch (InterruptedException e) {
                Logger.debug("***** Thread Finished *****" + e);
            }
        };

        Thread thread = new Thread(assignment);
        thread.start();
    }

    private void retryCall() {
        // de esta forma tomo como prioridad la llamada que mas tiempo lleva en espera
        callsWaiting.sort(Comparator.comparing(Call::getDateWaiting).reversed());
        System.out.print(callsWaiting);
        Call callRetry = callsWaiting.stream().filter(c -> c.getStatus().equals(CallStatus.WAITING))
                .findFirst().orElse(null);
        if (callRetry != null) {
            dispatchCall(callRetry);
        }
    }
}
