package com.almundo;

import com.almundo.domain.call.Call;
import com.almundo.domain.call.CallStatus;
import com.almundo.domain.employee.Employee;
import com.almundo.domain.employee.EmployeeStatus;
import com.almundo.domain.employee.EmployeeType;
import com.almundo.infrastructure.Dispatcher;
import com.almundo.services.EmployeeService;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;

import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author Kevin
 */
public class DispatcherTest {

    /**
     * Este Test prueba que al contestar una llamada se cambia el estado a ANSWERED
     */
    @Test
    public void answerCall() {
        Call call = new Call(10, CallStatus.WAITING, null);
        Employee employee = new Employee("Kevin", "CC", 1, EmployeeType.OPERATOR, EmployeeStatus.AVAILABLE);
        Dispatcher dispatcher = new Dispatcher();
        dispatcher.answerCall(call, employee);
        Assert.assertEquals(call.getStatus(), CallStatus.ANSWERED);
    }


    /**
     * Este Test prueba cuando hay 10 llamadas Entrantes y solo 3 empleados, y consiste en que cada empleado conteste una llamada a la vez
     * (las llamadas entrantes que no son contestadas en el momento pasan a estar en WAITING (espera))
     * y luego que termine esa llamada tome otra y asi sucesivamente hasta terminar todas las llamadas en espera
     */
    @Test
    public void tenCallInComingThreeEmployee() {

        List<Call> calls = createCalls(10);
        List<Employee> employees = createEmployees(3);
        EmployeeService.fillEmployees(employees);

        Dispatcher dispatcher = new Dispatcher();
        /* se empieza a ecorrer la lista de llamadas, las cuales van a ser contestadas por los 3 empleados,
        una vez cada empleado se desocupe vuelve a tomar otra llamada (esto se hace mediante la llamada del metodo retryCall)
         */
        IntStream.range(0, calls.size())
                .forEach(idx -> {
                            dispatcher.dispatchCall(calls.get(idx));
                            if (idx < 3) {
                                Assert.assertEquals(calls.get(idx).getStatus(), CallStatus.ANSWERED);
                            } else {
                                Assert.assertEquals(calls.get(idx).getStatus(), CallStatus.WAITING);
                            }

                        }
                );
    }

    /**
     * Este Test prueba cuando hay 10 llamadas Entrantes y 10 empleados,
     * por lo tanto todas las llamadas van a ser contestadas a la vez
     */
    @Test
    public void tenCallInComingTenEmployee() throws InterruptedException {
        List<Call> calls = createCalls(12);
        List<Employee> employeeList = createEmployees(10);

        EmployeeService.fillEmployees(employeeList);
        Dispatcher dispatcher = new Dispatcher();

        calls.stream().map(dispatcher::dispatchCall).collect(Collectors.toList());
        Assert.assertEquals(10, calls.stream().filter(call -> call.getStatus().equals(CallStatus.ANSWERED)).count());
    }

    /**
     * Este Test prueba cuando hay mas de 10 llamadas Entrantes y 10 empleados,
     * por lo tanto van a ser atendidas 10 llamadas y las demas pasaran a estar en espera hasta que el empleado este disponible
     * y pueda tomar otra llamada
     */
    @Test
    public void callMaxTenForTenEmployee() {
        List<Call> calls = createCalls(15);
        List<Employee> employeeList = createEmployees(10);
        EmployeeService.fillEmployees(employeeList);
        Dispatcher dispatcher = new Dispatcher();

         /* se empieza a ecorrer la lista de llamadas, las cuales van a ser contestadas por los 10 empleados,
         las llamadas que no son contestadas pasan a estado WAITING
         */
        IntStream.range(0, calls.size())
                .forEach(idx -> {
                            dispatcher.dispatchCall(calls.get(idx));
                            if (idx < 10) {
                                Assert.assertEquals(calls.get(idx).getStatus(), CallStatus.ANSWERED);
                            } else {
                                Assert.assertEquals(calls.get(idx).getStatus(), CallStatus.WAITING);
                            }

                        }
                );
    }


    private List<Call> createCalls(int numberOfCalls) {
        return IntStream.range(0, numberOfCalls)
                .mapToObj(i -> {
                    Random r = new Random();
                    return new Call((r.nextInt(5 + 1) + 5), CallStatus.INCOMING, null);
                }).collect(Collectors.toList());
    }

    private List<Employee> createEmployees(int numberOfEmployess) {
        return IntStream.rangeClosed(1, numberOfEmployess)
                .mapToObj(i -> new Employee(
                        "Empleado " + i,
                        "CC",
                        i + 100,
                        EmployeeType.getEmployeeType(i <= 3 ? i + "" : 1 + ""),
                        EmployeeStatus.AVAILABLE)
                ).collect(Collectors.toList());

    }
}
