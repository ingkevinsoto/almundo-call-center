package com.almundo;

import com.almundo.domain.employee.Employee;
import com.almundo.domain.employee.EmployeeStatus;
import com.almundo.domain.employee.EmployeeType;
import com.almundo.services.EmployeeService;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kevin
 */

public class EmployeeServiceTest {

    // Encuentra Un empleado de tipo Operator disponible, así todos los empleados de los demás tipos estén disponible
    @Test
    public void getAvailableEmployeeOperator() {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee("José", "CC", 1, EmployeeType.OPERATOR, EmployeeStatus.AVAILABLE));
        employees.add(new Employee("Ivan", "CC", 2, EmployeeType.SUPERVISOR, EmployeeStatus.AVAILABLE));
        employees.add(new Employee("Kevin", "CC", 3, EmployeeType.MANAGER, EmployeeStatus.AVAILABLE));

        EmployeeService.fillEmployees(employees);
        Employee availableEmployee = EmployeeService.findAvailableEmployee();
        Assert.assertEquals(availableEmployee.getEmployeeType(), EmployeeType.OPERATOR);
    }

    // Prueba que escoge el empleado de tipo Supervisor cuando no hay empleados de tipo Operador disponibles asi haya Manager sisponibles
    @Test
    public void getAvailableEmployeeSupervisor() {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee("José", "CC", 1, EmployeeType.OPERATOR, EmployeeStatus.BUSY));
        employees.add(new Employee("Ivan", "CC", 2, EmployeeType.SUPERVISOR, EmployeeStatus.AVAILABLE));
        employees.add(new Employee("Kevin", "CC", 3, EmployeeType.MANAGER, EmployeeStatus.AVAILABLE));

        EmployeeService.fillEmployees(employees);
        Employee availableEmployee = EmployeeService.findAvailableEmployee();
        Assert.assertEquals(availableEmployee.getEmployeeType(), EmployeeType.SUPERVISOR);
    }

    // Prueba que escoge un Manager cuando no hay empleados de tipo operator y Supervisor disponible
    @Test
    public void getAvailableEmployeeManager() {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee("José", "CC", 1, EmployeeType.OPERATOR, EmployeeStatus.BUSY));
        employees.add(new Employee("Ivan", "CC", 2, EmployeeType.SUPERVISOR, EmployeeStatus.BUSY));
        employees.add(new Employee("Kevin", "CC", 3, EmployeeType.MANAGER, EmployeeStatus.AVAILABLE));

        EmployeeService.fillEmployees(employees);
        Employee availableEmployee = EmployeeService.findAvailableEmployee();
        Assert.assertEquals(availableEmployee.getEmployeeType(), EmployeeType.MANAGER);
    }

}
